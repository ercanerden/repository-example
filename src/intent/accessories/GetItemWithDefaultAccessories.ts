import {ItemAccessoriesIntent} from "./ItemAccessoriesIntent";
import {Item} from "../../data/model/Item";

export class GetItemWithDefaultAccessoriesIntent extends ItemAccessoriesIntent {

    public async run(sku: string): Promise<Item> {
        let item = await this.itemRepository.getItem(sku, this.itemChannels);
        let showcaseLabel =  await this.itemRepository.populateLabel(item.getShowcaseLabel(), this.itemChannels);
        item.accessoryLabels.unshift(showcaseLabel);
        return item;
    }

}
