import {CartItem} from "../../model/cart/CartItem";
import {Cart} from "../../model/cart/Cart";

export interface CartRepository {

    getCart(): Promise<Cart>;
    getCartItems(): Promise<CartItem[]>;
    emptyCart(): Promise<Cart>;
    addToCart(sku: string[]): Promise<Cart>;
    removeFromCart(sku: string[]): Promise<Cart>;
    saveForLater(skus: string[]): Promise<Cart>;

}
