import {CartIntent} from "./CartIntent";
import {Cart} from "../../data/model/cart/Cart";

export class EmptyCartIntent extends CartIntent {

    public run(): Promise<Cart> {
        return this.cartRepository.emptyCart();
    }

}
