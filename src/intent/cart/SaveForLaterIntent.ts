import {CartIntent} from "./CartIntent";
import {Cart} from "../../data/model/cart/Cart";

export class SaveForLaterIntent extends CartIntent {

    public async run(skus:string[]) : Promise<Cart> {
        await this.cartRepository.saveForLater(skus);
        return this.cartRepository.removeFromCart(skus);
    }

}
