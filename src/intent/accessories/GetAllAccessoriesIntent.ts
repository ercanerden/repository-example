import {ItemAccessoriesIntent} from "./ItemAccessoriesIntent";
import {Item} from "../../data/model/Item";

export class GetAllAccessoriesIntent extends ItemAccessoriesIntent {

    public async run(sku: string): Promise<Item> {

        let item =  await this.itemRepository.getItem(sku, this.itemChannels);
        const showcaseLabel = item.getShowcaseLabel();
        item.accessoryLabels.unshift(showcaseLabel);
        let labels = await this.itemRepository.populateLabels(item.accessoryLabels, this.itemChannels);
        item.accessoryLabels = labels;
        return item;

    }

}

