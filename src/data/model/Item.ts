export class Item {
    public sku: string;
    accessoryLabels: AccessoryLabel[];

    // Scan labels for items to create a showcase label
    public getShowcaseLabel(): AccessoryLabel {
        return new AccessoryLabel();
    }

}

export class AccessoryLabel {
    title: string;
    items: Item[];
}
