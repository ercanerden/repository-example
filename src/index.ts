import {GetCustomAccessoriesIntent} from "./intent/accessories/GetCustomAccessoriesIntent";

import {Response} from "./net/Response";
import {Request} from "./net/Request";
import {GetItemWithDefaultAccessoriesIntent} from "./intent/accessories/GetItemWithDefaultAccessories";
import {GetAllAccessoriesIntent} from "./intent/accessories/GetAllAccessoriesIntent";
import {DefaultItemRepository} from "./data/repository/item/DefaultItemRepository";
import {AddToCartIntent} from "./intent/cart/AddToCartIntent";
import {DefaultCartRepository} from "./data/repository/cart/DefaultCartRepository";
import {EmptyCartIntent} from "./intent/cart/EmptyCartIntent";
import {GetCartIntent} from "./intent/cart/GetCartIntent";

function getLabelRequestType(labels: string[]): LabelRequestType {

    if (labels.length == 0) {
        return LabelRequestType.Default;
    } else if (labels.length == 1 && labels[0] == "all") {
        return LabelRequestType.All;
    } else {
        return LabelRequestType.Custom;
    }

}

enum LabelRequestType {
    Default,
    All,
    Custom
}

function addToCart(sku:string,  request: Request, response: Response) {
    let intent = new AddToCartIntent(new DefaultCartRepository());
    let result = intent.run(sku);
    response.write(result);
}

function emptyCart(request: Request, response: Response) {
    let intent = new EmptyCartIntent(new DefaultCartRepository());
    let result = intent.run();
    response.write(result);
}

function getCart(request: Request, response: Response) {
    let intent = new GetCartIntent(new DefaultCartRepository());
    let result = intent.run();
    response.write(result);
}

//  -------------------  Some controller method ------------
/**
 *  Fetch the item with the provided SKU and populate its accessory labels with items.
 *
 *  Labels are populated depending on what's passed in oprional `labels` parameter:
 *
 *  * If nothing or an empty array is provided only the showcase label items will be populated.
 *  * If an array with a single string `'all'` is provided, all labels and the showcase label will be populated.
 *  * For other values of the labels array, only the requested labels will be populated.
 *
 */
function getItemWithAccessories(sku:string, labelNames: string[], request: Request, response: Response) {

    let result: any;

    switch (getLabelRequestType(labelNames)) {

        case LabelRequestType.Default:
            result = new GetItemWithDefaultAccessoriesIntent([], new DefaultItemRepository()).run(sku);
            break;

        case LabelRequestType.All:
            result = new GetAllAccessoriesIntent([], new DefaultItemRepository()).run(sku);
            break;

        case LabelRequestType.Custom:
            result = new GetCustomAccessoriesIntent([], new DefaultItemRepository()).run(sku, labelNames);
            break;

    }

    console.log("Writing result...");
    response.write(result);

}

getItemWithAccessories("abc", [], new Request(), new Response());

