import {CartRepository} from "./CartRepository";
import {Cart} from "../../model/cart/Cart";
import {CartItem} from "../../model/cart/CartItem";

export class TestCartRepository implements CartRepository {

    async getCart(): Promise<Cart> {
        return new Cart();
    }

    async getCartItems(): Promise<CartItem[]> {
        return [];
    }

    async emptyCart(): Promise<Cart> {
        return new Cart();
    }

    async addToCart(sku: string[]): Promise<Cart>{
        return new Cart();
    }

    async removeFromCart(sku: string[]): Promise<Cart> {
        return new Cart();
    }

    async saveForLater(skus: string[]): Promise<Cart> {
        return new Cart();
    }

}