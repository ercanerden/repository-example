import {ItemAccessoriesIntent} from "./ItemAccessoriesIntent";
import {AccessoryLabel, Item} from "../../data/model/Item";

export class GetCustomAccessoriesIntent extends ItemAccessoriesIntent {

    public async run(sku: string, labelNames: string[]): Promise<Item> {

        let item = await this.itemRepository.getItem(sku, this.itemChannels);
        const customLabels = this.filterCustomLabels(labelNames, item.accessoryLabels);
        await this.itemRepository.populateLabels(customLabels, this.itemChannels);
        return item;

    }

    // Extract requested labels from the item
    private filterCustomLabels(labelNames: string[], labels: AccessoryLabel[]): AccessoryLabel[] {
        return labels.filter(value => labelNames.find(value2 => value2 == value.title));
    }

}
