import {Savings} from "../savings/Savings";

export class CartItem {
    sku: string;
    name: string;
    price: number;
    warranty: number;
    savings?: Savings[]
}

