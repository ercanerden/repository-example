import {AccessoryLabel, Item} from "../../model/Item";

export interface ItemRepository {

    getItem(sku: string, channels?: string[]): Promise<Item>;

    getItems(skus: string[], channels: string[]): Promise<Item[]>;

    populateLabel(label: AccessoryLabel, channels: string[]): Promise<AccessoryLabel>;

    populateLabels(labels: AccessoryLabel[], channels: string[]): Promise<AccessoryLabel[]>;

}