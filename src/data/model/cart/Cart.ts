import {CartItem} from "./CartItem";
import {CartTotals} from "./CartTotals";
import {SaveForLater} from "./SaveForLater";

export class Cart {
    items: CartItem[];
    saveForLater: SaveForLater;
    totals: CartTotals;
}
