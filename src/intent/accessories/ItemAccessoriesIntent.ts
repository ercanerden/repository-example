import {ItemRepository} from "../../data/repository/item/ItemRepository";

export abstract class ItemAccessoriesIntent {
    constructor(protected itemChannels: string[] = [], protected itemRepository: ItemRepository) { }
}
