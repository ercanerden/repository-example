export class CartTotals {
    subTotal: number;
    tax: number;

    public get total(): number {
        return this.subTotal + this.tax;
    }

}

