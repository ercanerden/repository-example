import {CartIntent} from "./CartIntent";
import {Cart} from "../../data/model/cart/Cart";
import {GetCartIntent} from "./GetCartIntent";

export class SaveCartForLater extends CartIntent {

   public async run(): Promise<Cart> {
       const cart = await this.cartRepository.getCart();
       const cartItemSkus  = cart.items.map(value => value.sku);
       await this.cartRepository.saveForLater(cartItemSkus);
       return this.cartRepository.emptyCart();
   }

}