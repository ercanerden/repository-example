import {CartIntent} from "./CartIntent";
import {Cart} from "../../data/model/cart/Cart";

export class GetCartIntent extends CartIntent {

    public run(): Promise<Cart> {
        return this.cartRepository.getCart();
    }

}