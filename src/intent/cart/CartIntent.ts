import {CartRepository} from "../../data/repository/cart/CartRepository";

export abstract class CartIntent {
    constructor(protected cartRepository: CartRepository) {}
}