import {CartIntent} from "./CartIntent";
import {Cart} from "../../data/model/cart/Cart";

export class AddToCartIntent extends CartIntent {
    public run(sku: string): Promise<Cart> {
        return this.cartRepository.addToCart([sku]);
    }
}