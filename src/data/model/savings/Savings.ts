import {SavingsItem} from "./SavingsItem";

export class Savings {
    free?: SavingsGroup[];
    instantSaving: SavingsGroup[];
}

export class SavingsGroup {
    name: string;
    items: SavingsItem[];
}