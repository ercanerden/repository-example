export class SavingsItem {
    sku: string;
    name: string;
    price: number;
    warranty: number;
}
