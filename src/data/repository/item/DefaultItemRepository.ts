import {ItemRepository} from "./ItemRepository";
import {AccessoryLabel, Item} from "../../model/Item";

export class DefaultItemRepository implements ItemRepository {

    public async getItem(sku: string, channels: string[] = []): Promise<Item> {
        return new Item();
    }

    public async getItems(skus: string[], channels: string[]): Promise<Item[]> {
        return [];
    }

    public async populateLabel(label: AccessoryLabel, channels: string[]): Promise<AccessoryLabel> {

        return this.getItems(label.items.map(item => item.sku), channels)
                          .then(items => {label.items = items;
                              return label;
                          });
    }

    public async populateLabels(labels: AccessoryLabel[], channels: string[]): Promise<AccessoryLabel[]> {
        let promises = [];

        for (let a of labels){
            promises.push(this.populateLabel(a, channels));
        }

        return Promise.all(promises);

    }

}